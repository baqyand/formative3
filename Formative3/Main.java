public class Main {

    public static void main(String[] args){
        //inisialisasi data
        //saat kita ingin mengubah lingkarang maka kita memakai decorator yang di timpa oleh class nya
        //di dalam class dekorator, kita menambahkan parameter namun isi parameternya dengan nama class
        //sehingga menghasilkan output tambahan jika di bandingkan dengan object biasa.

        BangunDatar lingkaran = new Lingkaran();

        BangunDatar lingkaranMerah = new WarnaDecorator(new Lingkaran());

        BangunDatar persegiMerah = new WarnaDecorator(new Persegi());

        System.out.println("Lingkaran tanpa border");
        lingkaran.bentuk();

        System.out.println("\nLingkaran di Border merah");
        persegiMerah.bentuk();

        System.out.println("\nPersegi di Border merah");
        lingkaranMerah.bentuk();
    }

}