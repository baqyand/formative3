public abstract class Decoratordesign implements BangunDatar{
    protected BangunDatar decoratedBangunDatar;

    public Decoratordesign(BangunDatar decoratorShape){
        this.decoratedBangunDatar = decoratorShape;
    }
    @Override
    public void bentuk() {
        decoratedBangunDatar.bentuk();
    }
}

class WarnaDecorator extends Decoratordesign{
    public WarnaDecorator(BangunDatar bd){
        super(bd);
    }

    @Override
    public void bentuk() {
        decoratedBangunDatar.bentuk();
        setWarna(decoratedBangunDatar);
    }
    private void setWarna(BangunDatar bangunDatar){
        System.out.println("Warna nya : merah");
    }
}

interface BangunDatar{
    public void bentuk();
}
class Lingkaran implements BangunDatar{
    @Override
    public void bentuk() {
        System.out.println("Bentuk: Lingkaran ");
    }
}
class Persegi implements BangunDatar{
    @Override
    public void bentuk() {
        System.out.println("Bentuk: Persegi");
    }
}
