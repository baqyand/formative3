public class Main {

    public static void main(String[] args) {
	// write your code here
        //ini merupakan contoh template pattern di mana class parent membuat abstract method
        //kemudian method tersebut wajib di override oleh kelas turunannya
        //saat sudah di override oleh class turunanya maka kita bisa memanggilnya di class main
        //template ini menguntungkan karena jika terjadi kesalahan logic di suatu method maka kita hanya perlu
        //membuka class yang bermasalah tersebut (jika isi di dalam classnya copas)
        //kita hanya perlu mengoverride dari parent class yang berbentuk template
        Simerah merah = new Simerah();
        System.out.println(merah.getNama());

        Sibiru biru = new Sibiru();
        System.out.println(biru.getNama());



    }
}
