public abstract class MiniMarket {
    protected String warna;

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }
    public abstract String getNama();
}
