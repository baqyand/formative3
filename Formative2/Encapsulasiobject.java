public class Encapsulasiobject {
    //membuat metode statis yang akan di gunakan olen main class
    //ini merupakan kelas factory yang mana membuat object untuk di gunakan di mamin class
    public static Pendidikan newInstance(String type){
        if (type.equalsIgnoreCase("smp")){
            return new Smp();
        }else if (type.equalsIgnoreCase("Sma")){
            return new Sma();
        }
        return null;
    }
}
class main{
    public static void main(String[] args) {
        //inisialisasi data dari class encapsulasiObject
        //kemudian kita memasukan parameter di dalam method new instance
        Pendidikan pendidikan = Encapsulasiobject.newInstance("smp");
        Pendidikan pendidikan1 = Encapsulasiobject.newInstance("sma");

        //ketika kita mau membandingkan apakah hasil nya benar atau salah maka kita menggunakan
        //instance of.//
        //jika benar maka nilai akan true
        //jika salah maka akan false
        // encapsulasi memudahkan kita untuk melakukan pengecekan data saat membuat program oop
        System.out.println(pendidikan instanceof Smp);
        System.out.println(pendidikan1 instanceof Sma);
    }
}
class Pendidikan{
    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
class Smp extends Pendidikan{
    private String nama;

    @Override
    public String getNama() {
        return nama;
    }
}
class Sma extends Pendidikan {
    private String nama;

    @Override
    public String getNama() {
        return nama;
    }
}

