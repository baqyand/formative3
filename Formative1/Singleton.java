public class Singleton {
    public static void main(String[] args) {
        //saat kita membuat singleton kita hanya akan menciptakan 1 instance saja
        //object ini akan sering di pakai di object lain
        //*Pattern ini bagaikan kelas Konfigurasi pada sebuah aplikasi, dimana semua hal yang
//        *berhubungan dengan settingan konfigurasi, di set sekali di class tersebut dan semua class
//        *lain yang membutuhkan instans konfigurasi semuanya merujuk pada instans kelas Konfigurasi nya.
        Abc obj1 = Abc.getInstance();
        Abc obj2 = Abc.getInstance();
        Abc obj3 = Abc.getInstance();
        /*
         *   Output yang tercipta seharusnya tiga kali, tetapi
         *   hanya tercipta satu kali..
         */

    }
}

class Abc {
    static Abc obj1 = new Abc();

    private Abc() {
        System.out.println("object terbuat ");
    }
    public static Abc getInstance(){
        return obj1;
    }
}